import { BrowserRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom'
import React, { useEffect, useRef, useState } from 'react'
import axios from 'axios'

export default function App() {
    const [redirect, setRedirect] = useState<boolean>(true)

    return (
        <BrowserRouter>
            {redirect ? <Navigate to={'/login/step-1'} /> : ''}
            <header className='h-20 bg-primary flex items-center p-4'>
                <h1 className='text-3xl text-black'>Sign Up</h1>
            </header>
            <main className='flex flex-col p-4 h-full'>
                <Routes>
                    <Route path='/login/*' element={<LoginPage setRedirect={setRedirect} />} />
                </Routes>
            </main>
        </BrowserRouter>
    )
}

interface LogPageType {
    setRedirect: (path: boolean) => void
}

let LoginPage: React.FC<LogPageType> = ({ setRedirect }) => {
    const [email, setEmail] = useState<string>('')
    const [disabled, setDisabled] = useState<boolean>(true)
    const [checked, setChecked] = useState<boolean>(false)
    const [btnTimer, setBtnTimer] = useState<string>('Hold to proceed')

    const [model, setModel] = useState<{ message: string, visible: boolean }>({ message: '', visible: false })
    const [loading, setLoading] = useState(false)
    const timerRef = useRef<NodeJS.Timeout | null>(null)

    const navigate = useNavigate()


    useEffect(() => {
        if (email.match('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$') && checked) {
            setDisabled(false)
        } else {
            setDisabled(true)
        }
    }, [email, checked])

    let submitTimerRun = () => {
        let startTime = Date.now()
        let now = Date.now()
        timerRef.current = setInterval(() => {
            now = Date.now()
            let remaining_time = 500 - (now - startTime)
            if (remaining_time <= 0) {
                clearInterval(timerRef.current as NodeJS.Timeout)
                timerRef.current = null
                setBtnTimer('Hold to proceed')
                setRedirect(false)
                navigate('/login/step-2')

            } else {
                setBtnTimer((remaining_time).toString() + ' ms')
            }
        }, 1)
    }
    let submitTimerStop = () => {
        if (timerRef.current) {
            clearInterval(timerRef.current)
            timerRef.current = null
            setBtnTimer('Hold to proceed')
        }
    }

    let Confirm = async () => {
        setLoading(true)
        await axios.post('http://localhost:4040/endpoint', { email: email }).then(r => {
            setLoading(false)
            if (r.status === 200)
                setModel({ message: 'Success', visible: true })
            else
                setModel({ message: 'Error', visible: true })
        })
    }
    return (
        <Routes>
            <Route path={'/step-1'} element={
                <>
                    <FormInput email={email} setEmail={setEmail} />
                    <div className='p-1'></div>
                    <FormCheckbox checked={checked} setChecked={setChecked} />
                    <button className='btn btn-primary mt-auto'
                            onMouseDown={() => {
                                submitTimerRun()
                            }}
                            onMouseUp={() => {
                                submitTimerStop()
                            }}
                            disabled={disabled}>{btnTimer}</button>
                </>
            } />

            <Route path={'/step-2'} element={
                <>
                    <Model model={model} setModel={setModel} />
                    <div className={'w-100 p-2 bg-white rounded text-lg'}>{email}</div>
                    <div className='p-1'></div>
                    <div className={[loading ? 'flex' : 'hidden', 'size-full justify-center items-center'].join(' ')}>
                        <svg
                            className='animate-spin h-10 w-10 mr-3 border-4 border-blue-300 border-b-blue-900 rounded-full '
                            viewBox='0 0 24 24'></svg>
                    </div>
                    <div className={'w-100 flex-auto flex space-x-4'}>
                        <button className='w-40 btn btn-outline mt-auto' onClick={() => navigate(-1)}>Back</button>
                        <button className='w-40 btn btn-primary mt-auto' onClick={() => Confirm()}>Confirm</button>
                    </div>
                </>
            } />
        </Routes>
    )
}

interface FormCheckBoxType {
    checked: boolean,
    setChecked: (val: boolean) => void
}

const FormCheckbox: React.FC<FormCheckBoxType> = ({ checked, setChecked }) => {
    return (
        <div className='form-control'>
            <label className='label cursor-pointer justify-start gap-2'>
                <input type='checkbox' className='checkbox checkbox-primary'
                       onChange={(e) => setChecked(e.target.checked)}
                       checked={checked} />
                <span className='label-text'>I agree</span>
            </label>
        </div>
    )
}

interface FormInputType {
    email: string,
    setEmail: (val: string) => void
}

const FormInput: React.FC<FormInputType> = ({ email, setEmail }) => {
    return (
        <label className='form-control'>
            <div className='label'>
                <span className='label-text'>Email</span>
            </div>
            <input type='text' placeholder='Type here' className='input'
                   onChange={(e) => setEmail(e.target.value)}
                   value={email} />
            {/* <div className="label">
                <span className="label-text-alt">Helper text</span>
            </div> */}
        </label>
    )
}

interface ModelType {
    model: { message: string, visible: boolean },
    setModel: (model: { message: string, visible: boolean }) => void
}

const Model: React.FC<ModelType> = ({ model, setModel }) => {
    return (
        <div
            className={[model.visible ? 'block' : 'hidden', 'fixed inset-0 overflow-y-auto bg-gray-800 bg-opacity-75 flex justify-center items-center'].join(' ')}>
            <div className='bg-white rounded-lg p-8 max-w-md w-full text-center relative'>
                <p className='text-lg text-gray-700'>{model.message}</p>
                <button
                    className={'absolute -top-4 -right-4 bg-red-600 text-white p-2 w-10   cursor-pointer rounded-full'}
                    onClick={() => setModel({ message: '', visible: false })}>x
                </button>
            </div>
        </div>
    )
}